export const MOCK_TOP_HEADLINGS = [
    {
        "source": {
            "id": null,
            "name": "E24.no"
        },
        "author": "Camilla Knudsen",
        "title": "Norges Bank holder renten på null, varsler heving i september - E24",
        "description": "Tiden med rekordlave renter er sannsynligvis snart forbi, selv om det fortsatt er usikkerhet knyttet til nye virusvarianter. – Det må skje noe veldig overraskende for at vi ikke skal få renteheving i september, sier DNB-økonom.",
        "url": "https://e24.no/boers-og-finans/i/dnJ8Ew/norges-bank-beholder-nullrente-varsler-heving-i-september",
        "urlToImage": "https://smp.vgc.no/v2/images/4831550c-87bc-4911-a050-17839aef82a3?fit=crop&h=1349&w=1900&s=c2748488c73c71144d3e96d79a5637453d00f680",
        "publishedAt": "2021-08-19T08:20:51Z",
        "content": "Tiden med rekordlave renter er sannsynligvis snart forbi, selv om det fortsatt er usikkerhet knyttet til nye virusvarianter. Det må skje noe veldig overraskende for at vi ikke skal få renteheving i s… [+3797 chars]"
    },
    {
        "source": {
            "id": "nrk",
            "name": "NRK"
        },
        "author": "Hedda Stølen, Helge Carlsen, Yama Wolasmal",
        "title": "Biden: USA forlater ikke Afghanistan før alle amerikanere er ute - NRK",
        "description": "Joe Biden lover at alle amerikanere skal være trygt ute av Afghanistan før troppene deres forlater landet. Storbritannia frykter de ikke vil kunne fullføre sin egen evakuering hvis USA trekker seg for tidlig ut, melder The Guardian.",
        "url": "https://www.nrk.no/urix/biden_-usa-forlater-ikke-afghanistan-for-alle-amerikanere-er-ute-1.15615520",
        "urlToImage": "https://gfx.nrk.no/uxwKxHny6pdVclHrPB5CvA6Sq2DThZ2v60IK1do1FTQg.jpg",
        "publishedAt": "2021-08-19T08:18:41Z",
        "content": "Siste: \r\nUSAs president Joe Biden sier at USA kommer til å fortsette å ha sine tropper i Afghanistan, helt til hver eneste amerikaner er ute av landet.\r\nDet er mellom 10.000 til 15.000 amerikanske bo… [+4092 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Dagbladet.no"
        },
        "author": "Maren Mjelstad",
        "title": "Nye funn i Pompeii overrasker - Dagbladet.no",
        "description": "Menneskelige levninger funnet i grav skal være de best bevarte noensinne.",
        "url": "https://www.dagbladet.no/nyheter/nye-funn-i-pompeii-overrasker/74121978",
        "urlToImage": "https://www.dagbladet.no/images/74122191.jpg?imageId=74122191&panow=78.934817170111&panoh=100&panox=0&panoy=0&heightw=27.522935779817&heighth=100&heightx=35.168195718654&heighty=0&width=1200&height=630",
        "publishedAt": "2021-08-19T07:26:29Z",
        "content": "Levningene av den tidligere slaven Marcus Venerius Secundio er funnet gravlagt i Pompeii. De delvis mumifiserte levningene på nesten to tusen år inkluderer både hår og bein, opplyser The Guardian.\r\nD… [+2656 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Dagbladet.no"
        },
        "author": "Didrik Søreide Kjær",
        "title": "Ole Gunnar Solskjær - Solskjær med rørende gest til Paul (37) - Dagbladet.no",
        "description": "Ole Gunnar Solskjær lover å innfri en døende Manchester United fans siste ønske.",
        "url": "https://www.dagbladet.no/sport/solskjaer-med-rorende-gest-til-paul-37/74125349",
        "urlToImage": "https://www.dagbladet.no/images/74125411.jpg?imageId=74125411&panow=100&panoh=50.714285714286&panox=0&panoy=7.1428571428571&heightw=100&heighth=100&heightx=0&heighty=0&width=1200&height=630",
        "publishedAt": "2021-08-19T07:26:27Z",
        "content": "Tidligere denne sommeren fikk familiefar Paul Walker beskjeden man aldri ønsker å få. Legene fant ut at han hadde kreft i magen og deres prognoser ga han bare fem måneder igjen å leve, skriver Belfas… [+1730 chars]"
    },
    {
        "source": {
            "id": "aftenposten",
            "name": "Aftenposten"
        },
        "author": "Hilde Lundgaard, Jørgen Arnor G. Lom",
        "title": "Flyttestrømmen ut av Oslo øker – nedgang i folketallet - Aftenposten",
        "description": "Flyttebølgen ut av Oslo er mer enn et blaff. Også de ferskeste flyttetallene er rekordhøye. Etter år med vekst går folketallet i Oslo ned.",
        "url": "https://www.aftenposten.no/oslo/i/k6MzWa/flyttestroemmen-ut-av-oslo-oeker-nedgang-i-folketallet",
        "urlToImage": "https://premium.vgc.no/v2/images/30390554-3446-421e-9054-76764e12d668?fit=crop&h=1072&w=2048&s=855a75820733b9ad2cca628fca99529d24592911",
        "publishedAt": "2021-08-19T07:18:26Z",
        "content": "Flyttebølgen ut av Oslo er mer enn et blaff. Også de ferskeste flyttetallene er rekordhøye. Etter år med vekst går folketallet i Oslo ned.\r\n9591 mennesker flyttet ut av Oslo andre kvartal i år. Det e… [+512 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Abcnyheter.no"
        },
        "author": "NTB",
        "title": "Avis: Havanna-syndrom hos amerikanske diplomater i Berlin - ABC Nyheter",
        "description": "Minst to amerikanske diplomater i Tyskland har plager som peker mot en mystisk sykdom kjent som Havanna-syndromet, skriver Wall Street Journal.",
        "url": "https://www.abcnyheter.no/nyheter/verden/2021/08/19/195780860/avis-havanna-syndrom-hos-amerikanske-diplomater-i-berlin",
        "urlToImage": "https://imaginary.abcmedia.no/resize?width=980&interlace=true&url=https%3A%2F%2Fimaginary.abcmedia.no%2Fpipe%3Furl%3Dhttps%253A%252F%252Fabcnyheter.drpublish.aptoma.no%252Fout%252Fimages%252Farticle%252F%252F2021%252F08%252F19%252F195780877%252F1%252Foriginal%252F49156928.jpg",
        "publishedAt": "2021-08-19T06:38:12Z",
        "content": "Den amerikanske avisen viser til ikke navngitte amerikanske diplomater som sier de to aktuelle personene har symptomer som svimmelhet, hodepine, smerter i ørene, slapphet og søvnmangel.\r\nSaken, som o… [+764 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Finansavisen.no"
        },
        "author": "Odd Steinar Parr",
        "title": "Sterke tall – sjefen er stålbull - Finansavisen",
        "description": "Containerrederiet rir bølgen, og toppsjefen ser ganske så lyst på fremtiden. Guidingen heves kraftig.",
        "url": "https://finansavisen.no/nyheter/shipping/2021/08/19/7721088/sterke-tall-fra-mpc-container-ships",
        "urlToImage": "https://presizely.finansavisen.no/980x,q75,prog/https://hegnar.fotoware.cloud/fotoweb/embed/2021/08/5ff1f7cf3e714cf782eb8e313a5a9e2e@2.jpg",
        "publishedAt": "2021-08-19T06:37:16Z",
        "content": "MPC\r\nDet sterke containermarkedet gjenspeiler seg klart i MPC Container Ships-regnskapet. En time charter-inntjening (TCE) på i snitt 13.437 dollar pr. dag sørget for å løfte driftsinntektene med 75 … [+1738 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Direkte.vg.no"
        },
        "author": null,
        "title": "SSB: Fødselstallet økte med over 4 prosent i andre kvartal – VG Nå: Nyhetsdøgnet - VG",
        "description": "14.578 barn ble født mellom april og juni i år. Det er 575 flere enn på samme tid i fjor, viser nye tall fra Statistisk sentralbyrå (SSB).",
        "url": "https://direkte.vg.no/nyhetsdognet/news/ssb-foedselstallet-oekte-med-over-4-prosent-i-andre-kvartal.15BD2vv8g",
        "urlToImage": "https://1.vgc.no/vgnett-prod/img/vgLogoSquare.png?28042014-1",
        "publishedAt": "2021-08-19T06:12:21Z",
        "content": null
    },
    {
        "source": {
            "id": null,
            "name": "Dagbladet.no"
        },
        "author": null,
        "title": "Daniel Craig - Vil gjøre barna arveløse - Dagbladet.no",
        "description": "Filmstjerna ønsker ikke å videreføre formuen til framtidige generasjoner.",
        "url": "https://www.dagbladet.no/kjendis/vil-gjore-barna-arvelose/74125895",
        "urlToImage": "https://www.dagbladet.no/images/74124734.jpg?imageId=74124734&panow=100&panoh=46.540880503145&panox=0&panoy=5.6603773584906&heightw=44.761904761905&heighth=100&heightx=9.047619047619&heighty=0&width=1200&height=630",
        "publishedAt": "2021-08-19T06:04:53Z",
        "content": "Den verdensberømte skuespilleren Daniel Craig (53) er kanskje aller best kjent for sin rolle som «James Bond» i actionfilmene om 007-agenten.\r\nPå privaten har han den 29 år gamle dattera Ella med sin… [+3596 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Www.vg.no"
        },
        "author": "Vilde Elgaaen",
        "title": "«Outlander»-stjerne har fått barn – VG - VG",
        "description": "Skuespiller Caitríona Balfe (41) har fått sitt første barn med ektemannen Anthony McGill.",
        "url": "https://www.vg.no/rampelys/i/L5oOOq/outlander-stjerne-har-faatt-barn",
        "urlToImage": "https://akamai.vgc.no/v2/images/5b37d55d-5d6f-4daf-ad55-206511d689a8?fit=crop&h=1267&w=1900&s=b08b9b3d96a72fc207c174eb02a512e3939672db",
        "publishedAt": "2021-08-19T06:02:00Z",
        "content": "MOR: Caitriona Balfe avslører på Instagram at hun har fått en sønn. Foto: Frazer Harrison / GETTY IMAGES NORTH AMERICA\r\nSkuespiller Caitríona Balfe (41) har fått sitt første barn med ektemannen Antho… [+1651 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Www.dn.no"
        },
        "author": "Erik Bucher Johannessen",
        "title": "Underskuddet øker i Nel – svakere enn ventet i andre kvartal - Dagens Næringsliv",
        "description": "Underskuddet øker i Nel – svakere enn ventet i andre kvartal",
        "url": "https://www.dn.no/bors/nel/jon-andre-lokke/dn-aksjer/underskuddet-oker-i-nel-svakere-enn-ventet-i-andre-kvartal/2-1-1053624",
        "urlToImage": "https://images.dn.no/image/VVJwanRhQmtlcWh5a3RwcFJtdXNsUVVja2FvQ1hTU3dlcVZadmlGSm9BMD0=/nhst/binary/f6f316c4747c8c66e35a977aaa07f8a2?image_version=1080",
        "publishedAt": "2021-08-19T06:00:00Z",
        "content": "Artikkelen fortsetter under annonsen\r\nSaken oppdateres.\r\nTorsdag morgen la hydrogenselskapet Nel frem tall for andre kvartal. Selskapet genererte en omsetning på 163,7 millioner kroner i kvartalet og… [+6034 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Abcnyheter.no"
        },
        "author": "NTB",
        "title": "VG: Bråthen får advokathjelp i arbeidstvisten med skiforbundet - ABC Nyheter",
        "description": "Sportssjef Clas Brede Bråthen har ifølge VG fått juridisk hjelp i den pågående arbeidskonflikten med Norges Skiforbund (NSF).",
        "url": "https://www.abcnyheter.no/nyheter/sport/2021/08/19/195780832/vg-brathen-far-advokathjelp-i-arbeidstvisten-med-skiforbundet",
        "urlToImage": "https://imaginary.abcmedia.no/resize?width=980&interlace=true&url=https%3A%2F%2Fimaginary.abcmedia.no%2Fpipe%3Furl%3Dhttps%253A%252F%252Fabcnyheter.drpublish.aptoma.no%252Fout%252Fimages%252Farticle%252F%252F2021%252F08%252F19%252F195780833%252F1%252Foriginal%252F49152739.jpg",
        "publishedAt": "2021-08-19T05:31:23Z",
        "content": "NSF ønsker ikke å fornye kontrakten med hoppernes sportssjef når hans kontrakt går ut etter denne sesongen.\r\nAvisen har fått opplyst at Bråthen nå får advokathjelp og derfor ikke ønsker å stille til … [+613 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Nettavisen.no"
        },
        "author": "Stian Andrè De Wahl",
        "title": "Ekspertene hyller Rekdal etter HamKam-forvandling: - Helt ellevilt - Nettavisen",
        "description": "Kjetil Rekdal har forvandlet HamKam og gjort dem til en opprykkskandidat til Eliteserien. Nå hylles han av ekspertene.",
        "url": "https://www.nettavisen.no/sport/ekspertene-hyller-rekdal-etter-hamkam-forvandling-helt-ellevilt/s/5-95-277579",
        "urlToImage": "https://g.api.no/obscura/API/dynamic/r1/ece5/tr_2000_2000_s_f/1629321160000/nett/2021/8/18/23/49151239.jpg?chk=4BC606",
        "publishedAt": "2021-08-19T05:24:16Z",
        "content": "Kjetil Rekdal har forvandlet HamKam og gjort dem til en opprykkskandidat til Eliteserien. Nå hylles han av ekspertene. \r\n 19.08.21 07:24\r\n19.08.21 08:05\r\nEtter onsdagens 2-0-seier borte mot Jerv topp… [+2600 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Dagbladet.no"
        },
        "author": "Line Brustad",
        "title": "Truer bestanden: - Dødelig krepsepest påvist i ny elv - Dagbladet.no",
        "description": "Forrige uke ble den dødelige og svært smittsomme sykdommen krepsepest påvist i en ny norsk elv. Mattilsynet slår alarm, og innfører forskrift som retter seg mot enhver som ferdes i området.",
        "url": "https://www.dagbladet.no/nyheter/dodelig-krepsepest-pavist-i-ny-elv/74124287",
        "urlToImage": "https://www.dagbladet.no/images/74124344.jpg?imageId=74124344&panow=100&panoh=100&panox=0&panoy=0&heightw=100&heighth=100&heightx=0&heighty=0&width=1200&height=630",
        "publishedAt": "2021-08-19T05:18:24Z",
        "content": "For noen betyr høsten krepsing - det vil si at man har lov til å fange kreps, og i Norge er det tillatt å krepse i perioden 6. august - 14. september.\r\nMen hvordan denne høstens krepsesesong blir, er… [+4299 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "E24.no"
        },
        "author": "Camilla Knudsen",
        "title": "Lerøy bedrer driftsresultat, opprettholder slaktemål – E24 - E24",
        "description": "Sjømatselskapet venter at inntjeningen i tredje kvartal 2021 blir vesentlig bedre enn i andre kvartal 2021, selv om corona-usikkerheten fortsatt er stor.",
        "url": "https://e24.no/hav-og-sjoemat/i/47E8oE/leroey-bedrer-driftsresultat-opprettholder-slaktemaal",
        "urlToImage": "https://smp.vgc.no/v2/images/65bde94f-7da2-45ca-be1a-b89d79bac701?fit=crop&h=1267&w=1900&s=7e88f43ca3f6048506400078c498fba4bdf6abf9",
        "publishedAt": "2021-08-19T05:14:42Z",
        "content": "Sjømatselskapet venter at inntjeningen i tredje kvartal 2021 blir vesentlig bedre enn i andre kvartal 2021, selv om corona-usikkerheten fortsatt er stor.\r\nLerøy-sjef Henning Beltestad presenterer tor… [+2830 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Nettavisen.no"
        },
        "author": "Sjur Tveito",
        "title": "Premier League-rykter, Premier League | Torsdagens Premier League-rykter - Nettavisen",
        "description": "Det hevdes at Manchester City forbereder et monsterbud på Harry Kane, men samtidig skal de lyseblå ha funnet ut hvilken spiller som vil være deres plan B.",
        "url": "https://www.nettavisen.no/sport/torsdagens-premier-league-rykter/s/12-95-3424168951",
        "urlToImage": "https://g.api.no/obscura/API/dynamic/r1/nadp/tr_2000_2000_s_f/1629351284297/2021/08/19/3424168959/1/original/49152110.jpg?chk=20A0A8",
        "publishedAt": "2021-08-19T05:12:59Z",
        "content": "Det hevdes at Manchester City forbereder et monsterbud på Harry Kane, men samtidig skal de lyseblå ha funnet ut hvilken spiller som vil være deres plan B. \r\n 19.08.21 07:12\r\n19.08.21 07:34\r\nMancheste… [+2876 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Www.tv2.no"
        },
        "author": "TV 2 AS",
        "title": "Nye regler i Storbritannia - fullvaksinerte nordmenn risikerer karantene - TV 2",
        "description": "En ny endring i Storbritannias innreiseregler kan bety trøbbel for mange nordmenn.",
        "url": "https://www.tv2.no/a/14172188/",
        "urlToImage": "https://www.cdn.tv2.no/images/14172295.jpg?imageId=14172295&panow=100&panoh=51.020408163265&panox=0&panoy=18.367346938776&heightw=100&heighth=100&heightx=0&heighty=0&width=1200&height=630",
        "publishedAt": "2021-08-19T05:05:30Z",
        "content": "Til glede for mange har Storbritannia nå åpnet for karantenefri innreise fra hele Europa, så lenge man er fullvaksinert.\r\nEn fersk endring i det britiske regelverket kan derimot skape trøbbel. Person… [+4275 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Nettavisen.no"
        },
        "author": "Stig Martin Solberg",
        "title": "Dette skriver avisene om norsk politikk torsdag 19. august. - Nettavisen",
        "description": "Full oversikt på ett minutt: Dette skriver avisene om norsk politikk torsdag 19. august.",
        "url": "https://www.nettavisen.no/nyheter/dette-skriver-avisene-om-norsk-politikk-torsdag-19-august/s/12-95-3424168928",
        "urlToImage": "https://g.api.no/obscura/API/dynamic/r1/nadp/tr_2000_2000_s_f/1629355726297/2021/08/19/3424168956/1/original/48930717.jpg?chk=C347DD",
        "publishedAt": "2021-08-19T05:01:59Z",
        "content": "Full oversikt på ett minutt: Dette skriver avisene om norsk politikk torsdag 19. august. \r\n 19.08.21 07:01\r\n19.08.21 08:48\r\n12 på topp torsdag\r\nSenterpartiet vil prøve å stjele håpløse Høyre-velgere … [+19071 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "Www.tv2.no"
        },
        "author": "TV 2 AS",
        "title": "Mener Kelly ledet et kriminelt nettverk - TV 2",
        "description": "Påtalemyndigheten langet ut mot «overgriperen» R. Kelly under rettssakens første dag.",
        "url": "https://www.tv2.no/a/14172404/",
        "urlToImage": "https://www.cdn.tv2.no/images/14172602.jpg?imageId=14172602&panow=100&panoh=100&panox=0&panoy=0&heightw=100&heighth=100&heightx=0&heighty=0&width=1200&height=630",
        "publishedAt": "2021-08-19T04:35:07Z",
        "content": "Ifølge påtalemyndighetene er R. Kelly en kynisk overgrepsmann som utnyttet stjernestatusen til å lure jenter, gutter og unge kvinner, for så å mishandle dem. Kellys forsvarere sier klienten deres er … [+3166 chars]"
    },
    {
        "source": {
            "id": null,
            "name": "E24.no"
        },
        "author": "Eivind Bøe, Kjetil Malkenes Hovland",
        "title": "Oljefondet ble med kinesiske giganter på børs før kursras - E24",
        "description": "Blant de største børsnoteringene Oljefondet ble med på i første halvår, finnes flere kinesiske teknologiselskaper. I etterkant har kinesiske myndigheter tatt grep, og kursene har rast.",
        "url": "https://e24.no/boers-og-finans/i/28vk5r/oljefondet-ble-med-kinesiske-giganter-paa-boers-foer-kursras",
        "urlToImage": "https://smp.vgc.no/v2/images/85730bb7-14c0-4e50-b743-7e3b068979ee?fit=crop&h=1267&w=1900&s=80ae40cafa31f68f7a8f11f973a6826286b83b68",
        "publishedAt": "2021-08-19T04:12:45Z",
        "content": "Blant de største børsnoteringene Oljefondet ble med på i første halvår, finnes flere kinesiske teknologiselskaper. I etterkant har kinesiske myndigheter tatt grep, og kursene har rast. \r\nKINA-PRESS: … [+5178 chars]"
    }
].map(article => ({
    ...article,
    id: Math.random().toString(24).slice(2)
}))