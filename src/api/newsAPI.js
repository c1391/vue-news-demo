import { MOCK_TOP_HEADLINGS } from "../mocks/top-headlines.mock";

/*
 * @param country
 * @param category
 */
export function getTopHeadlines(country, category) {

    // return mock data for development
  if (process.env.VUE_APP_USE_MOCKS == true) {
    return new Promise((resolve) => {
      resolve(MOCK_TOP_HEADLINGS);
    });
  }



  return fetch(
    `https://newsapi.org/v2/top-headlines?country=${country}&category=${category}`,
    {
      headers: {
        Authorization: process.env.VUE_APP_NEWS_API_KEY,
      },
    }
  )
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Could not fetch the news 😭");
      }

      return response.json();
    })
    .then((response) => {
      return response.articles.map((article) => ({
        ...article,
        id: Math.random()
          .toString(24)
          .slice(2),
      }));
    });
}
